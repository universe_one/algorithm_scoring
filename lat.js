// create a function that accept two string parameter
// your function should be able to determine the first parameter is anagram with from the second parameter vice versa
// anagram will happen if the amount of each letter from first parameter is exactly equal with the second parameter and vice versa
// examples:
// anagram('aaz', 'zza') => false
// anagram('anagram', 'nagaram')) => true

const anagram = (a, b) => {
  a = a.replace(/[^\w]/g, "").toLowerCase(); // Hilangkan karakter yg bukan alfabet dg regex dan convert ke huruf kecil
  b = b.replace(/[^\w]/g, "").toLowerCase();

  //   if (sortString(a) === sortString(b)) {
  //     return console.log(true);
  //   } else {
  //     return console.log(false);
  //   }

  let charA = getChar(a);
  let charB = getChar(b);

  for (let char in charA) {
    if (charA[char] !== charB[char]) {
      return console.log(false);
    }
  }
  return console.log(true);
};

const getChar = (str) => {
  let x = {};

  for (let char of str) {
    x[char] = x[char] + 1 || 1;
  }
  console.log(x);
  return x;
};

// const sortString = (str) => {
//   //return str.split("").sort().join("");
// };

anagram("aaz", "zza");
anagram("anagram", "nagaram");
