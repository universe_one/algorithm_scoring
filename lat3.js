let a = [1, 2, 2, 2, 2, 4, 5];
let b = [1, 2, 3, 4, 4, 4, 4, 4, 5, 5, 5, 7, 7, 8, 9];
let c = [3, 4, 5, 5, 5, 6, 6, 7, 7, 7, 8, 8, 8, 8, 9, 10, 10, 11];

const countUniqueValues = (arr) => {
  return new Set(arr).size;
};

const countUniqueValues2 = (arr) => {
  return arr.filter((element, index, array) => array.indexOf(element) === index)
    .length;
};

console.log(countUniqueValues(a));
console.log(countUniqueValues(b));
console.log(countUniqueValues2(c));
